# A Simple Load Balanced Time Service

A simple QUEUE with channels available to do the task. 

The logic is simple: Fifo for channels. 

# Trade-off
- Unnecessary memory use because the Queue
- Use mutex to avoid concurrency push/pop
- harder to test


# TODO Improvements
- Use a buffered channel instead Queue
    - Make test easier
    - Use less memory than Queue
    - Concurrency "transparently" 
- Create a go routine supervisor to deal with channels errors
- Give channels priority based on AvgResponseTime
- Improve tests

# CI

https://circleci.com/bb/xild/sixt/
