package main

import (
	"testing"
	"fmt"
	"time"
)

var manager = &TimeServiceManager{}

func TestMyLoadBalancer_Request(t *testing.T) {
	type fields struct {
		queue *Queue
	}
	tests := []struct {
		name                  string
		fields                fields
		want                  []TimeService
		timesExecutedExpected int
	}{
		{name: "Running 1 time request", want: []TimeService{manager.Spawn()}, timesExecutedExpected: 1},
		{name: "Running 4 time requests", want: []TimeService{manager.Spawn(), manager.Spawn(), manager.Spawn()}, timesExecutedExpected: 3},
	}
	for _, tt := range tests {
		timesExecuted := 0
		t.Run(tt.name, func(t *testing.T) {
			lb := &MyLoadBalancer{
				queue: tt.fields.queue,
			}

			for _, request := range tt.want {
				lb.RegisterInstance(request.ReqChan)
				go request.Run()
				select {
				case rsp := <-lb.Request(nil):
					fmt.Println(rsp)
					timesExecuted = timesExecuted + 1
				case <-time.After(5 * time.Second):
					t.Errorf("%v timeout", tt.name)
				}

			}

			if timesExecuted != tt.timesExecutedExpected {
				t.Errorf("Executed %v times instead %v expected", timesExecuted, tt.timesExecutedExpected)

			}

		})
	}
}

func TestMyLoadBalancer_RegisterInstance(t *testing.T) {
	type fields struct {
		queue *Queue
	}
	tests := []struct {
		name         string
		fields       fields
		ch           []chan Request
		expectedSize int
	}{
		{name: "Register a new instance in Queue", ch: []chan Request{manager.Spawn().ReqChan}, expectedSize: 1},
		{name: "Register a four new instances in Queue", ch: []chan Request{manager.Spawn().ReqChan, manager.Spawn().ReqChan, manager.Spawn().ReqChan, manager.Spawn().ReqChan}, expectedSize: 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			lb := &MyLoadBalancer{
				queue: tt.fields.queue,
			}
			for _, instance := range tt.ch {
				lb.RegisterInstance(instance)
			}

			if lb.queue.isEmpty() {
				t.Errorf("MyLoadBalancer.RegisterInstance is empty")
			}

			if len(lb.queue.values) != tt.expectedSize {
				t.Errorf("MyLoadBalancer.RegisterInstance.queue %v is different than expected %v", len(lb.queue.values), tt.expectedSize)
			}
		})
	}
}
